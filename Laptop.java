public class Laptop{
	
	private String make;
	private String processor;
	private int ram;
	
	public Laptop(String make, String processor, int ram){
		this.make = make;
		this.processor = processor;
		this.ram = ram;
	}
	
	public void setMake(String newMake){
		this.make = newMake;
	}
	
	public String getMake(){
		return this.make;
	}
	
	public String getProcessor(){
		return this.processor;
	}
	
	public void setRam(int newRam){
		this.ram = newRam;
	}
	
	public int getRam(){
		return this.ram;
	}
	
	public void fullDetails(String make, String processor, int ram){
		
		System.out.println("The updated " + make + " laptop has " + ram + " GB of RAM and " + processor + " as processor");
	}
}