import java.util.Scanner;

public class Shop{
	
	public static void main (String[] args){
		
		Scanner scan = new Scanner(System.in);
		
		Laptop[] pc = new Laptop[4];
		
		for(int i = 0; i < pc.length; i++){
			
			System.out.println("Enter the computer's information as follow, pressing enter each time a field is entered: make, processor, RAM. For computer " + (i+1));
			pc[i] = new Laptop(scan.nextLine(), scan.nextLine(), scan.nextInt());
			scan.nextLine();
		}
		
		System.out.println();
		System.out.println("The last laptop is from " + pc[3].getMake());
		System.out.println("The last laptop has " + pc[3].getProcessor() + " as processor");
		System.out.println("The last laptop has " + pc[3].getRam() + " GB of RAM");
		
		System.out.println();
		System.out.println("Please enter laptop's new make");
		pc[3].setMake(scan.nextLine());
		System.out.println("Please enter laptop's new RAM");
		pc[3].setRam(scan.nextInt());
		
		System.out.println();
		pc[3].fullDetails(pc[3].getMake(), pc[3].getProcessor(), pc[3].getRam());
	}
}